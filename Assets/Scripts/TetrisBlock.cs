﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TetrisBlock : MonoBehaviour
{
    public Vector3 rotationPoint;
    private float previousTime;
    public static int height = 20;
    public static int width = 10;
    private static Transform[,] grid = new Transform[width, height];
    public int points = 0;
    int LineFound = 0;
    public bool gameEnd = false;
    public GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindWithTag("spawner").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow)) //Press left, block moves left
        {
            transform.position += new Vector3(-1, 0, 0);
            if (!ValidMove())
                transform.position -= new Vector3(-1, 0, 0);

        }
        else if (Input.GetKeyDown(KeyCode.RightArrow)) //Press right, block moves right
        {
            transform.position += new Vector3(1, 0, 0);
            if (!ValidMove())
                transform.position -= new Vector3(1, 0, 0);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow)) //Press up, block turns
        {
            //rotate!
            transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), 90);
            if (!ValidMove())
                transform.RotateAround(transform.TransformPoint(rotationPoint), new Vector3(0, 0, 1), -90);
        }

        if (Time.time - previousTime > (Input.GetKey(KeyCode.DownArrow) ? gameManager.falltime / 10 : gameManager.falltime)) //Press down, block moves down faster
        {
            transform.position += new Vector3(0, -1, 0);
            if (!ValidMove())
            {
                transform.position -= new Vector3(0, -1, 0);
                AddToGrid();
                CheckIfLost();
                CheckForLines();

                this.enabled = false;
                FindObjectOfType<GameManager>().NewTetromino();
            }

            previousTime = Time.time;
        }

        if (gameManager.gameover) //If game is over, set totalPoints and empty the grid
        {
            //gameManager.totalPoints = points;
            EmptyGrid();
        }

        void EmptyGrid() //Empty the grid
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (grid[j, i] != null)
                    {
                        Destroy(grid[j, i].gameObject);
                        grid[j, i] = null;
                    }
                }
            }
        }

        void CheckForLines() //Check if there are full lines, then delete, move rows down and add to line counter
        {
            for (int i = height - 1; i >= 0; i--)
            {
                if (HasLine(i))
                {
                    LineFound++;
                    gameManager.totalLines++;
                    Debug.Log("Line grew by one. Total: " + LineFound);
                    DeleteLine(i);
                    RowDown(i);
                }
            }

            if (LineFound > 0)
            {
                Debug.Log("Lines after going through lines: " + LineFound);
                GivePoints(LineFound); //Give points
                Debug.Log("LINES BEFORE UPDATING LEVEL: " + gameManager.totalLines);
                gameManager.UpdateLevel(gameManager.totalLines); //Update the level based on lines cleared
            }
        }

        void CheckIfLost() //Check if there are blocks in the highest row, if so, end game
        {
            for (int i = 0; i < width; i++)
            {
                if (grid[i, 19] != null)
                {
                    Debug.Log("GAME OVER");
                    gameEnd = true;
                    gameManager.gameover = true;

                    //PlayerPrefs.SetInt("asd", gamemanager.totalPoints);
                }
            }
        }

        bool HasLine(int i) //Are there blocks on this row
        {
            for (int j = 0; j < width; j++)
            {
                if (grid[j, i] == null)
                    return false;
            }
            return true;
        }

        void DeleteLine(int i) //Delete a single found line after detecting it - goes through process after each single line until reaches full amount of lines
        {
            for (int j = 0; j < width; j++) //Delete line
            {
                Destroy(grid[j, i].gameObject);
                grid[j, i] = null;
            }
        }

        void GivePoints(int p)
        {
            if (p < 4) //if less than 4 lines, give 1000 totalPoints per line
            {
                points = points + (1000 * p);
                Debug.Log("Less than 4 lines");
            }
            else //if tetris (4 lines), give 5000 totalPoints
            {
                points = points + 5000;
                Debug.Log("more than 4 lines");
            }

            Debug.Log(LineFound + "lines");
            gameManager.totalPoints = gameManager.totalPoints + points; // same as: gameManager.totalPoints += points
            Debug.Log(points + "totalPoints in TetrisBlock");
            LineFound = 0;
        }

        void RowDown(int i) //Move the row down
        {
            for (int y = i; y < height; y++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (grid[j, y] != null)
                    {
                        grid[j, y - 1] = grid[j, y];
                        grid[j, y] = null;
                        grid[j, y - 1].transform.position -= new Vector3(0, 1, 0);
                    }
                }
            }
        }

        void AddToGrid() //Add blocks to the grid
        {
            foreach (Transform children in transform)
            {
                int roundedX = Mathf.RoundToInt(children.transform.position.x);
                int roundedY = Mathf.RoundToInt(children.transform.position.y);

                grid[roundedX, roundedY] = children;
            }
        }

        bool ValidMove() //Is it possible to move the block this way
        {
            foreach (Transform children in transform)
            {
                int roundedX = Mathf.RoundToInt(children.transform.position.x);
                int roundedY = Mathf.RoundToInt(children.transform.position.y);
                if (roundedX < 0 || roundedX >= width || roundedY < 0 || roundedY >= height)
                {
                    return false;
                }

                if (grid[roundedX, roundedY] != null)
                    return false;
            }

            return true;
        }
    }
}