﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RocketScript : MonoBehaviour
{
    public GameObject spawner;
                                    //Sprite scales if tinyRocket is default
    public Sprite tinyRocket;       //1 x 1
    public Sprite tinyRocketFire;   //1.6 x 3.3

    public Sprite smallRocket;
    public Sprite smallRocketFire;

    public Sprite mediumRocket;
    public Sprite mediumRocketFire;

    public Sprite bigRocket;
    public Sprite bigRocketFire;

    public Sprite castle;
    public Sprite castleFire;

    //How many points you need to get each scene

    public int tinyRocketPoints = 30000;
    public int smallRocketPoints = 50000; //Jos muutat sitä tässä, niin se ei oikeasti muutu, pidä mielessä!
    public int mediumRocketPoints = 70000;
    public int bigRocketPoints = 100000;
    public int castlePoints = 120000;

    Vector3 originalPos;

    // Start is called before the first frame update
    void Start()
    {
        originalPos = gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawner.GetComponent<GameManager>().rocketLaunched)
        {
            //Debug.Log("ROCKET MOVING");
            Vector3 rocketMovement = new Vector3(0, 10, 0);

            Debug.Log("Total points for launch: " + spawner.GetComponent<GameManager>().totalPoints);

            if (spawner.GetComponent<GameManager>().totalPoints >= smallRocketPoints)
            {
                LaunchSmall();
            }
            if (spawner.GetComponent<GameManager>().totalPoints >= mediumRocketPoints)
            {
                LaunchMedium();
            }
            if (spawner.GetComponent<GameManager>().totalPoints >= bigRocketPoints)
            {
                LaunchBig();
            }
            if (spawner.GetComponent<GameManager>().totalPoints >= castlePoints)
            {
                LaunchCastle();
            }
            else
            {
                LaunchTiny();
            }
            
            rocketMovement *= Time.deltaTime;
            transform.Translate(rocketMovement);
        }
    }

    public void RestartRocket()
    {
        GetComponent<Image>().sprite = tinyRocket;
        gameObject.transform.position = originalPos;
        transform.localScale = new Vector3(1f, 1f, 0);
        Debug.Log("Rocket has restarted!");
        spawner.GetComponent<GameManager>().totalPoints = 0;
    }

    public void LaunchTiny()
    {
        GetComponent<Image>().sprite = tinyRocketFire;
        transform.localScale = new Vector3(1.6f, 3.3f, 0);
    }

    public void LaunchSmall()
    {
        GetComponent<Image>().sprite = smallRocketFire;
    }

    public void LaunchMedium()
    {
        GetComponent<Image>().sprite = mediumRocketFire;
    }

    public void LaunchBig()
    {
        GetComponent<Image>().sprite = bigRocketFire;
    }

    public void LaunchCastle()
    {
        GetComponent<Image>().sprite = castleFire;
    }

    public void ChangeCorrectSprite()
    {
        Debug.Log("CHANGING SPRITE");
        if (spawner.GetComponent<GameManager>().totalPoints >= smallRocketPoints)
        {
            Debug.Log("SMALL SPRITE");
            GetComponent<Image>().sprite = smallRocket;
        }
    }
}
