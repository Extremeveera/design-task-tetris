﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject[] Tetrominoes;
    public TetrisBlock tetrisBlock;
    public bool gameover = false;
    public GameObject CanvasObject; //game end screen
    public GameObject gameStartScreen;
    public int totalPoints = 0;
    public GameObject maingameUI;
    public Text levelText;
    public bool gameStarted = false;
    private SpriteRenderer tinyRocket;
    public float rocketLaunchTime = 3;
    float timer = 0;
    public bool rocketLaunched = false;
    public RocketScript rocketScript;
    public Text pointsText;
    public int totalLines = 0;
    public int level = 0;

    public float falltime = 0.8f;

    public bool Lv1 = false;
    public bool Lv2 = false;
    public bool Lv3 = false;
    public bool Lv4 = false;
    public bool Lv5 = false;
    public bool Lv6 = false;
    public bool Lv7 = false;
    public bool Lv8 = false;
    public bool Lv9 = false;

    // Start is called before the first frame update
    void Start()
    {
        levelText.text = level.ToString();
    }

    public void NewTetromino()
    {

        if (!gameover && gameStarted)
        {
            rocketLaunched = false;
            Instantiate(Tetrominoes[Random.Range(0, Tetrominoes.Length)], transform.position, Quaternion.identity); //Start making tetrominos
        }
        else if (gameover && gameStarted)
        {
            Debug.Log("Endscreen");
            CanvasObject.SetActive(true); //Make gameend screen appear
            
            //Turn score to zero and save high score
        }
    }
    
    void Update()
    {
        pointsText.text = totalPoints.ToString();

        if (gameover && !rocketLaunched)
        {
            rocketScript.ChangeCorrectSprite(); //Changes the rocket's starting sprite to the correct one

            timer += Time.deltaTime;
            if (timer > rocketLaunchTime)
            {
                Debug.Log("ROCKET LAUNCH!");
                timer = 0;
                rocketLaunched = true; //resets in NewTetromino and info is received by RocketScript
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && gameover) //Starting a new game
        {
            rocketScript.RestartRocket();
            Debug.Log("SPACE");
            CanvasObject.SetActive(false); //Turn off gameend screen
            Debug.Log("Total Lines: " + totalLines);
            gameover = false;
            Debug.Log("Total totalPoints: " + totalPoints);

            NewTetromino();
        }

        if (Input.GetKeyDown(KeyCode.Space) && !gameStarted)
        {
            gameStartScreen.SetActive(false);
            gameStarted = true;
            NewTetromino();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            Debug.Log("Points in GameManager: " + totalPoints);
        }

        //LEVELS!
        //formula for speed depending on level
        //UI lvl text is being updated
    }

    public void UpdateLevel(int l)
    {
        //LVL 1
        if (l >= 10 && l < 19) //Lines between 10 and 19 = LVL 1
        {
            level = 1;
            levelText.text = level.ToString();
            Lv1 = true;
            falltime = 0.75f;
        }

        //LVL 2
        if (l >= 20 && l < 29) //Lines between 20 and 29 = LVL 1
        {
            level = 2;
            levelText.text = level.ToString();
            Lv2 = true;
            falltime = 0.7f;
        }

        //LVL 3
        if (l >= 30 && l < 39)
        {
            level = 3;
            levelText.text = level.ToString();
            Lv3 = true;
            falltime = 0.6f;
        }

        //LVL 4
        if (l >= 40 && l < 49) 
        {
            level = 4;
            levelText.text = level.ToString();
            Lv4 = true;
            falltime = 0.5f;
        }

        //LVL 5
        if (l >= 50 && l < 59) 
        {
            level = 5;
            levelText.text = level.ToString();
            Lv5 = true;
            falltime = 0.4f;
        }

        //LVL 6
        if (l >= 60 && l < 69) 
        {
            level = 6;
            levelText.text = level.ToString();
            Lv6 = true;
            falltime = 0.3f;
        }

        //LVL 7
        if (l >= 70 && l < 79) 
        {
            level = 7;
            levelText.text = level.ToString();
            Lv7 = true;
            falltime = 0.2f;
        }

        //LVL 8
        if (l >= 80 && l < 89) 
        {
            level = 8;
            levelText.text = level.ToString();
            Lv8 = true;
            falltime = 0.1f;
        }

        //LVL 9
        if (l >= 90 && l < 99) 
        {
            level = 9;
            levelText.text = level.ToString();
            Lv9 = true;
            falltime = 0.05f;
        }
    }
}
